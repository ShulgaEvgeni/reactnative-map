import React from 'react';
import { makeAutoObservable, runInAction } from 'mobx';

import apiTransports from '../services/transoprt.service'
import { transoprt } from '../types/transport';

class TransportStore {
    transports: transoprt[] = []
    currentTransportId: number | null = null
    filter: { 'cargo': boolean, 'passenger': boolean, 'special_transport': boolean } = {
        cargo: true,
        passenger: true,
        special_transport: true
    }

    setTransportId = (id: number) => {
        this.currentTransportId = id
    }

    getTransports = async (options?: { MapData?: MapData, filter?: { 'cargo': boolean, 'passenger': boolean, 'special_transport': boolean, } }) => {
        const res = await apiTransports.getTransports(options)
        runInAction(() => {
            this.transports = res
        })
    }

    getTransportById = async (id: number): Promise<transoprt | null> => {
        const res = await apiTransports.getTransportById(id)
        if (res)
            return res
        else
            return null
    }

    deleteTransport = () => {
        runInAction(() => {
            this.transports = []
        })
    }

    setFilter = (filter: { 'cargo': boolean, 'passenger': boolean, 'special_transport': boolean, }) => {
        runInAction(() => {
            this.filter = filter
        })
    }

    constructor() {
        makeAutoObservable(this);
    }
}

interface MapData {
    latitude: number;
    latitudeDelta: number;
    longitude: number;
    longitudeDelta: number;
}

export const transportStore = new TransportStore();

export const TransportStoreStoreContext = React.createContext(transportStore);
export const useTransportStore = () => React.useContext(TransportStoreStoreContext)