export default {
    title: 'ТС:',
    categoryts: 'Категория ТС:',
    nameDriver: 'Имя водителя:',
    phone: 'Телефон:',
    buttons: {
        call: 'Позвонить',
        message: 'Написать'
    },
    message: 'Добрый день, подскажите пожалуйста, какой номер заказа у вас сейчас в работе'
}