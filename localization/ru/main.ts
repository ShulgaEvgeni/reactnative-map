export default {
    menu: {
        setting: 'Настройки',
        filter: {
            title: 'Фильтр транспрорта',
            button: 'Применить',
            cargo: 'Грузовой',
            passenger: 'Пассажирский',
            special_transport: 'Спецтранспорт'
        }
    },
    infoMap: {
        categoryts: 'Категория ТС:',
        nameDriver: 'Имя водителя:',
        namets: 'Название ТС:',
        button: 'Подробнее'
    },
    map: {
        title: 'Карта ТС'
    },
    list: {
        title: 'Список ТС',
        categoryts: 'Категория ТС:',
        nameDriver: 'Имя водителя:',
        namets: 'Название ТС:'
    }
}