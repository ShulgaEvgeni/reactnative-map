import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import ru from './ru';
import en from './en';
const resources = {
    ru,
    en
};
i18n.use(initReactI18next)
    .init({
        compatibilityJSON: 'v3',
        resources,
        lng: 'ru',
        interpolation: {
            escapeValue: false,
        },
        detection: {
            order: ['localStorage', 'cookie'],
            cache: ['localStorage', 'cookie']
        }
    });
export default i18n;