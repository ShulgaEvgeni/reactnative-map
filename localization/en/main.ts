export default {
    menu: {
        setting: 'Settings',
        filter: {
            title: 'Transport filter',
            button: 'Apply',
            cargo: 'Cargo',
            passenger: 'Passenger',
            special_transport: 'Special transport'
        }
    },
    infoMap: {
        categoryts: 'Vehicle category:',
        nameDriver: 'Driver\'s name:',
        namets: 'Vehicle name:',
        button: 'More'
    },
    map: {
        title: 'Transport map'
    },
    list: {
        title: 'List of vehicles',
        categoryts: 'Vehicle category:',
        nameDriver: 'Driver\'s name:',
        namets: 'Vehicle name:'
    }
}