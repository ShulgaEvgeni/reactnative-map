import main from './main';
import setting from './setting';
import transport from './transport';
import languages from '../languages';

export default {
    main,
    setting,
    transport,
    languages
};