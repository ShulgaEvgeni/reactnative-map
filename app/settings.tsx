

import React, { useState } from 'react';
import { StyleSheet, View } from 'react-native'
import { Picker } from '@react-native-picker/picker';
import { useTranslation } from 'react-i18next';
import { Stack } from 'expo-router';


export default () => {
  const { t, i18n } = useTranslation('setting')
  const [selectedLanguage, setSelectedLanguage] = useState(i18n.language);

  const changeLanguage = (value: string) => {
    setSelectedLanguage(value)
    i18n.changeLanguage(value)
  }

  return (
    <>
      <Stack.Screen options={{ title: t('title') }} />
      <View style={styles.container}>
        <Picker
          style={{ width: 150 }}
          selectedValue={selectedLanguage}
          onValueChange={changeLanguage}>
          {pickerOptions.map((el, index) => <Picker.Item key={index} label={el.title} value={el.id} />)}
        </Picker>
      </View>
    </>
  );
}

const pickerOptions = [
  { title: 'Русский', id: 'ru' },
  { title: 'English', id: 'en' },
]

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});
