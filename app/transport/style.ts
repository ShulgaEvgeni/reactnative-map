import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1
    },
    map: {
        height: 300
    },
    infoBlock: {
        marginHorizontal: 20,
        marginTop: 30
    },
    phoneBlock: {
        marginHorizontal: 20,
        marginTop: 10
    },
    phoneBlock__buttons: {
        marginTop: 5,
        display: 'flex',
        flexDirection: 'row'
    },
    'phoneBlock__buttons-item': {
        marginRight: 5
    }
})