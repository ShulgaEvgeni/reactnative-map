import React, { useEffect, useState } from 'react';
import { Stack, useRouter, useNavigation } from 'expo-router';
import { View, Text, Linking } from 'react-native';
import { Button } from 'react-native-paper';

import Map from '../../components/map'
import { useTransportStore } from '../../store/transport.store';
import { transoprt } from '../../types/transport';

import styles from './style'
import { useTranslation } from 'react-i18next';

export default () => {
    const { t } = useTranslation('transport')
    const { currentTransportId, getTransportById } = useTransportStore()
    const [marker, setMarker] = useState<transoprt>()

    const router = useRouter()

    const message = t('message');


    useEffect(() => {
        if (currentTransportId)
            getTransportById(currentTransportId).then((res) => {
                if (res)
                    setMarker(res)
                else
                    router.back()
            })
    }, [])

    const handleCall = () => {
        Linking.openURL(`tel:${marker?.phone}`);
    };

    const handleWhatsApp = () => {
        Linking.openURL(`whatsapp://send?phone=${marker?.phone}&text=${encodeURIComponent(message)}`);
    };

    return (
        <>
            <View style={styles.container}>
                {
                    marker &&
                    <>
                        <Stack.Screen options={{ title: `${t('title')} ${marker.id}#${marker.typeTS}`, headerRight: undefined }} />
                        <Map
                            style={styles.map}
                            initialRegion={{
                                latitude: marker.coordinate.latitude,
                                longitude: marker.coordinate.longitude,
                                latitudeDelta: 0.009,
                                longitudeDelta: 0.009,
                            }}
                        >
                            <Map.Marker
                                coordinate={{ latitude: marker.coordinate.latitude, longitude: marker.coordinate.longitude }}
                            />
                        </Map>
                        <View style={styles.infoBlock}>
                            <Text>{`${t('categoryts')} ${marker.typeTS}`}</Text>
                            <Text>{`${t('nameDriver')} ${marker.firstname} ${marker.surname}`}</Text>
                        </View>
                        <View style={styles.phoneBlock}>
                            <Text>
                                {`${t('phone')} ${marker.phone}`}
                            </Text>
                            <View style={styles.phoneBlock__buttons}>
                                <Button
                                    mode='contained'
                                    onPress={handleCall}
                                    style={styles['phoneBlock__buttons-item']}
                                >
                                    {t('buttons.call')}
                                </Button>
                                <Button
                                    mode='contained'
                                    onPress={handleWhatsApp}
                                    style={styles['phoneBlock__buttons-item']}
                                >
                                    {t('buttons.message')}
                                </Button>
                            </View>
                        </View>
                    </>
                }
            </View>
        </>
    );
}
