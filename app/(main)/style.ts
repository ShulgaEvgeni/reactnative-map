import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 1
    },
    button: {
        position: 'absolute',
        bottom: 10,
        right: 10,
        zIndex: 5
    }
});


