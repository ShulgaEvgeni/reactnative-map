import React, { useState, useEffect } from 'react'
import { Stack, usePathname, useRouter } from 'expo-router';
import { StyleSheet } from 'react-native'
import { IconButton } from 'react-native-paper';
import Icon from "@expo/vector-icons/MaterialCommunityIcons";

import Menu from '../../module/menu'
import styles from './style';


export default () => {
  const router = useRouter();
  const pathname = usePathname();
  const [isMap, setIsMap] = useState(true)

  useEffect(() => {
    setIsMap(pathname === '/')
  }, [pathname])

  return (
    <>
      <Stack screenOptions={{
        headerRight: () => <Menu />
      }}>
        <Stack.Screen name='index' options={{ title: 'map' }} />
        <Stack.Screen name='list/index' options={{ title: 'list' }} />
      </Stack>
      <IconButton
        onPress={() => { router.push(isMap ? '/list' : '/'); setIsMap(v => !v) }}
        style={styles.button}
        icon={props => <Icon name={isMap ? 'format-list-bulleted' : 'map'} {...props} />}
        mode='contained'
      />
    </>
  );
}
