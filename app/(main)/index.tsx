import React, { useEffect, useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { observer } from 'mobx-react-lite';
import { Region } from 'react-native-maps';

import Map from '../../components/map';
import BottomSheet from '../../components/BottomSheet';
import { useTransportStore } from '../../store/transport.store';
import InfoTSforMap from '../../module/infoTSforMap';
import { transoprt } from '../../types/transport';
import { useTranslation } from 'react-i18next';
import { Stack } from 'expo-router';

const ts_cargo = require('../../assets/images/ts_cargo.png');
const ts_passenger = require('../../assets/images/ts_passenger.png');
const ts_special_transport = require('../../assets/images/ts_special_transport.png');

const initialRegion = {
    latitude: 59.943416,
    longitude: 30.332289,
    latitudeDelta: 0.3,
    longitudeDelta: 0.3,
}

export default observer(() => {
    const { t } = useTranslation('main')
    const {
        getTransports,
        deleteTransport,
        transports,
        filter
    } = useTransportStore()
    const [open, setOpen] = useState(false)
    const [region, setRegion] = useState(initialRegion)
    const [currentTS, setCurrentTS] = useState<transoprt | null>(null)

    useEffect(() => {
        getTransports({ MapData: region, filter })
    }, [region, filter])

    const openInfoMarker = async (data: transoprt) => {
        setCurrentTS(data)
        setOpen(true)
    }

    const closeBottomSheet = () => {
        setCurrentTS(null)
        setOpen(false)
    }

    const handleRegionChangeComplete = (region: Region) => {
        if (region.latitudeDelta < 1 || region.longitudeDelta < 1) {
            setRegion(region)
        } else {
            deleteTransport()
        }
    };

    return (
        <>
            <Stack.Screen options={{ title: t('map.title') }} />
            <View style={styles.container}>
                <Map
                    initialRegion={initialRegion}
                    onRegionChangeComplete={handleRegionChangeComplete}
                >
                    {transports && transports.map((el) => {
                        const typeTS = {
                            'cargo': ts_cargo,
                            'passenger': ts_passenger,
                            'special_transport': ts_special_transport
                        }

                        return <Map.Marker
                            key={el.id}
                            id={el.id.toString()}
                            icon={typeTS[el.typeTS]}
                            coordinate={{ latitude: el.coordinate.latitude, longitude: el.coordinate.longitude }}
                            onPress={() => { openInfoMarker(el) }}
                        />
                    }
                    )}

                </Map>
                <BottomSheet
                    open={open}
                    onClose={closeBottomSheet}
                    height={200}
                >
                    {currentTS &&
                        <InfoTSforMap {...currentTS} />
                    }
                </BottomSheet>
            </View>
        </>
    );
})

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    }
});
