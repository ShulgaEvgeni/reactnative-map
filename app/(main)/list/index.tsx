import React, { useState, useEffect } from 'react';
import { View, FlatList, Text, ActivityIndicator, Image } from 'react-native';
import { observer } from 'mobx-react-lite'
import Icon from "@expo/vector-icons/MaterialCommunityIcons";
import { IconButton } from 'react-native-paper';
import { Stack, useRouter } from 'expo-router';
import { useTranslation } from 'react-i18next';

import { useTransportStore } from '../../../store/transport.store';
import styles from './style'

const ts_cargo = require('../../../assets/images/ts_cargo.png')
const ts_passenger = require('../../../assets/images/ts_passenger.png')
const ts_special_transport = require('../../../assets/images/ts_special_transport.png')

export default observer(() => {
    const { t } = useTranslation('main')
    const router = useRouter();
    const {
        transports,
        filter,
        getTransports,
        setTransportId,
    } = useTransportStore()
    const [isLoading, setIsLoading] = useState(true);
    const [page, setPage] = useState(1);
    const [totalPages, setTotalPages] = useState(0);

    // Функция для обработки прокрутки до конца списка и загрузки следующей страницы
    const handleEndReached = () => {
        if (!isLoading && page < totalPages) {
            setPage((prevPage) => prevPage + 1);
        }
    };

    // Загрузка данных при монтировании компонента или изменении номера страницы
    useEffect(() => {

    }, [])

    useEffect(() => {
        getTransports({ filter })
        setIsLoading(false)
    }, [filter]);



    // Рендер элемента списка
    // @ts-ignore
    const renderItem = ({ item }) => {
        const typeTS = {
            'cargo': ts_cargo,
            'passenger': ts_passenger,
            'special_transport': ts_special_transport
        }

        const onPressButton = () => {
            setTransportId(item.id)
            router.push('/transport')
        }

        return (
            <>
                <Stack.Screen options={{ title: t('list.title') }} />
                <View style={styles.item}>
                    <View style={styles.item__info}>
                        <Image source={
                            // @ts-ignore
                            typeTS[item.typeTS]
                        } />
                        <View style={styles['item__info-text']}>
                            <Text>{`${t('list.namets')} ${item.id}#${item.typeTS}`}</Text>
                            <Text>{`${t('list.categoryts')} ${item.typeTS}`}</Text>
                            <Text>{`${t('list.nameDriver')} ${item.firstname} ${item.surname}`}</Text>
                        </View>
                    </View>
                    <IconButton
                        mode='contained'
                        icon={props => <Icon name='more' {...props} />}
                        onPress={onPressButton}
                    />
                </View>
            </>
        )
    };
    
    const renderFooter = () => {
        return isLoading ? (
            <ActivityIndicator size="large" color="gray" />
        ) : null;
    };

    return (
        <View style={{ flex: 1 }}>
            <FlatList
                style={styles.list}
                data={transports}
                renderItem={renderItem}
                keyExtractor={(item) => item.id.toString()}
                onEndReached={handleEndReached}
                onEndReachedThreshold={0.1}
                ListFooterComponent={renderFooter}
            />
        </View>
    );
});
