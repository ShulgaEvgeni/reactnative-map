import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    list: {
        flex: 1,
        marginTop: 20,
        marginBottom: 70
    },
    item: {
        marginBottom: 10,
        marginHorizontal: 15,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,

        elevation: 4,

        backgroundColor: '#c3c3c3',
        paddingHorizontal: 10,
        paddingVertical: 5,
        height: 70,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderRadius: 8
    },
    item__info: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    'item__info-text': {
        marginLeft: 10
    }
})