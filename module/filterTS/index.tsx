import React, { useEffect, useState } from 'react'
import { View, Text } from 'react-native';
import { observer } from 'mobx-react-lite'
import { Button, Switch } from 'react-native-paper';
import { useRouter } from 'expo-router';
import { useTranslation } from 'react-i18next';

import { useTransportStore } from '../../store/transport.store'
import { transoprt } from '../../types/transport'
import styles from './style'

export default observer((props: any) => {
    const { t } = useTranslation('main')

    const { setFilter, filter } = useTransportStore()
    const [open1, setOpen1] = useState(false)
    const [open2, setOpen2] = useState(false)
    const [open3, setOpen3] = useState(false)

    useEffect(() => {
        setOpen1(filter.cargo)
        setOpen2(filter.passenger)
        setOpen3(filter.special_transport)
    }, [])

    const onPressButton = () => {
        setFilter({
            cargo: open1,
            passenger: open2,
            special_transport: open3,
        })
    }

    return (
        <View style={styles.container}>
            <Text style={styles.title}>{t('menu.filter.title')}</Text>
            <View style={styles.switchBlock} >
                <Text>{t('menu.filter.cargo')}</Text>
                <Switch style={styles.switchBlock__switch} value={open1} onValueChange={() => { setOpen1(v => !v) }} />
            </View>
            <View style={styles.switchBlock}>
                <Text>{t('menu.filter.passenger')}</Text>
                <Switch style={styles.switchBlock__switch} value={open2} onValueChange={() => { setOpen2(v => !v) }} />
            </View>
            <View style={styles.switchBlock}>
                <Text>{t('menu.filter.special_transport')}</Text>
                <Switch style={styles.switchBlock__switch} value={open3} onValueChange={() => { setOpen3(v => !v) }} />
            </View>
            <Button
                style={styles.button}
                mode='contained'
                onPress={onPressButton}
            >
                {t('menu.filter.button')}
            </Button>
        </View>
    );
})
