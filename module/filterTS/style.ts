import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        paddingHorizontal: 30,
        paddingVertical: 20
    },
    title: {
        fontSize: 16,
        fontWeight: '600'
    },
    switchBlock: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    switchBlock__switch: {
        marginLeft: 10
    },
    button: {
        marginTop: 10
    }
});
