import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'flex-start',
        justifyContent: 'center',
        paddingHorizontal: 30,
        paddingVertical: 40
    },
    button: {
        marginTop: 20
    }
});
