import React from 'react'
import { View, Text } from 'react-native';
import { Button } from 'react-native-paper';
import { useRouter } from 'expo-router';
import { useTranslation } from 'react-i18next';

import { useTransportStore } from '../../store/transport.store'
import { transoprt } from '../../types/transport'
import styles from './style'

export default (props: transoprt) => {
    const { t } = useTranslation('main')
    const { setTransportId } = useTransportStore()
    const route = useRouter()

    const onPressButton = () => {
        setTransportId(props.id)
        route.push('/transport')
    }

    return (
        <View style={styles.container}>
            <View>
                <Text>{`${t('infoMap.namets')} ${props.id}#${props.typeTS}`}</Text>
                <Text>{`${t('infoMap.categoryts')} ${props.typeTS}`}</Text>
                <Text>{`${t('infoMap.nameDriver')} ${props.firstname} ${props.surname}`}</Text>
            </View>
            <Button
                style={styles.button}
                mode='contained'
                onPress={onPressButton}
            >
                {t('infoMap.button')}
            </Button>
        </View>
    );
}
