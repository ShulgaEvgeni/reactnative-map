import React, { useState } from 'react';
import { useRouter } from 'expo-router';
import Icon from '@expo/vector-icons/MaterialCommunityIcons';
import { Divider, IconButton, Menu } from 'react-native-paper';
import { useTranslation } from 'react-i18next';

import FilterTS from './filterTS'

export default () => {
    const { t } = useTranslation('main')
    const [open, setOpen] = useState(false)
    const router = useRouter();

    return (
        <Menu
            theme={'ligth'}
            style={{ transform: [{ translateY: 5 }, { translateX: 10 }] }}
            anchor={
                <IconButton
                    onPress={() => { setOpen(v => !v) }}
                    icon={(props) => <Icon name='menu' {...props} />}
                />
            }
            visible={open}
            onDismiss={() => { setOpen(v => !v) }}
            anchorPosition='bottom'
        >
            <Menu.Item onPress={() => { router.push('/settings'); setOpen(v => !v) }} title={t('menu.setting')} />
            <Divider />
            <FilterTS />
        </Menu>
    );
}
