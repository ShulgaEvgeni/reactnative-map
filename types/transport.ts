export type transoprt = {
    "id": number,
    "typeTS": typeTransoprt,
    "coordinate": {
        "latitude": number,
        "longitude": number
    },
    "status": statusTransoprt,
    "firstname": string,
    "surname": string,
    "phone": string | null
}

export type typeTransoprt = 'cargo' | 'passenger' | 'special_transport'
export type statusTransoprt = 'ride' | 'broken' | 'pending'