import { transoprt } from 'types/transport';
import data from './transport.mock.json'


// Эмитация запросов к api
export default {
    getTransports: (options?: { MapData?: MapData, filter?: { 'cargo': boolean, 'passenger': boolean, 'special_transport': boolean, } }): transoprt[] => {
        let temp = data.transports
        if (options) {
            if (options.MapData) { temp = filterVisiblePoints(options.MapData, data.transports) as any }
            if (options.filter) {
                const typeTS: string[] = []
                if (options.filter.cargo) typeTS.push('cargo')
                if (options.filter.passenger) typeTS.push('passenger')
                if (options.filter.special_transport) typeTS.push('special_transport')
                temp = temp.filter((el) => typeTS.includes(el.typeTS))
            }

        }
        return temp as any
    },
    getTransportById: (id: number): transoprt => {
        return data.transports.find(el => el.id === id) as any
    }
}


function filterVisiblePoints(mapData: MapData, points: Point[]): Point[] {
    const visiblePoints: Point[] = [];

    for (const point of points) {
        if (
            mapData.latitude - mapData.latitudeDelta <= point.coordinate.latitude &&
            point.coordinate.latitude <= mapData.latitude + mapData.latitudeDelta &&
            mapData.longitude - mapData.longitudeDelta <= point.coordinate.longitude &&
            point.coordinate.longitude <= mapData.longitude + mapData.longitudeDelta
        ) {
            visiblePoints.push(point);
        }
    }

    return visiblePoints;
}

interface MapData {
    latitude: number;
    latitudeDelta: number;
    longitude: number;
    longitudeDelta: number;
}

interface Point {
    coordinate: { latitude: number, longitude: number }
}