import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    bottomSheetContainer: {
        height: '100%',
        top: '100%',
        width: '100%',
        backgroundColor: 'white',
        position: 'absolute',
        borderRadius: 20,
        zIndex: 100
    },
    header: {
        width: '100%',
        height: 35,
    },
    headerButton: {
        height: 35,
        width: 35,
        marginRight: 15,
        marginLeft: 'auto'
    }
});


