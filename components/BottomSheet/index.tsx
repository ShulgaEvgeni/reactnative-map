import React, { useCallback, useEffect } from 'react';
import {
    Dimensions,
    View,
    ViewProps
} from 'react-native';
import Animated, {
    Extrapolate,
    interpolate,
    useAnimatedStyle,
    useSharedValue,
    withSpring,
} from 'react-native-reanimated';
import { IconButton } from "react-native-paper";
import Icon from "@expo/vector-icons/MaterialCommunityIcons";

import styles from './styles';

const { height: SCREEN_HEIGHT } = Dimensions.get('window');


type BottomSheetProps = {
    height?: number,
    open?: boolean,
    onClose?: () => void
};

export type BottomSheetRefProps = {
    scrollTo: (destination: number) => void;
    isActive: () => boolean;
};

const BottomSheet = (props: BottomSheetProps & ViewProps) => {
    const {
        open,
        children,
        height = SCREEN_HEIGHT - 100,
        onClose,
        style,
        ...view
    } = props
    const MAX_TRANSLATE_Y = -height;

    const translateY = useSharedValue(0);

    const scrollTo = useCallback((destination: number) => {
        'worklet';
        translateY.value = withSpring(destination, { damping: 50 });
    }, []);

    useEffect(() => {
        if (open)
            scrollTo(MAX_TRANSLATE_Y)
        else
            scrollTo(0)
    }, [open])

    const rBottomSheetStyle = useAnimatedStyle(() => {
        return {
            transform: [{ translateY: translateY.value }],
        };
    });

    return (
        <Animated.View style={[styles.bottomSheetContainer, rBottomSheetStyle, style]} {...view}>
            <View style={styles.header}>
                <IconButton
                    style={styles.headerButton}
                    icon={props => <Icon name="close" {...props} />}
                    onPress={() => {
                        if (onClose) onClose()
                    }}
                />
            </View>
            <View style={{ height: height - 35 }}>
                {children}
            </View>
        </Animated.View >
    );
};

export default BottomSheet;