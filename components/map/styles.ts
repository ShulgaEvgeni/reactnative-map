import { StyleSheet, Dimensions } from 'react-native';

export default StyleSheet.create({
    map: {
        flex: 1,
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height
    },
    markerImage: {
        height: 30,
        width: 30
    }
});