import React from 'react'
import MapView, { MapViewProps } from 'react-native-maps'

import Marker from "./components/marker";

import styles from './styles'

const Map = (props: MapViewProps) => {
    return (
        <MapView
            style={styles.map}
            provider='google'
            toolbarEnabled={false}
            {...props}
        >
            {props.children}
        </MapView>
    )
}

Map.Marker = Marker

export default Map
