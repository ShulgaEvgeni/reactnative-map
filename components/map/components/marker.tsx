import React from 'react'
import { MapMarker, MapMarkerProps } from 'react-native-maps'

type markerProps = {
    // iconStr?: string
}

const Map = (props: markerProps & MapMarkerProps) => {
    const { ...marker } = props

    return (
        <MapMarker
            description='des'
            {...marker}
        >

        </MapMarker>
    )
}

export default Map
